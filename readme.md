# Steps to run the script:

#### Data:
 - Unzip eyehome.zip to /home/username/eyehome
 - Start mongodb instance on default port
 - mongorestore /home/username/eyehome

After a few seconds, mongodb will have a database called “eyehome” with all the data collected as collections.

#### Code:
python chronoviz-data-creator.py