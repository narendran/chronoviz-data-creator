'''
Input: Data from Bob's gazepoints
Output: Three CSV files - 
- eyehome application used (start time, stop time, category, description)
- eye actions (start time, stop time, category, description)
-  gaze points (time, x, y)

Overview:
--------------
Read each gazepoint from database, and look for time gaps of more than 1 hour and then apply additional criteria to cutoff a session. 
For each session, create a folder under sessions directory
	- Add aforementioned three csv files to the directory.
'''

from pymongo import MongoClient
import datetime
import pytz
import os

''' DB initializations '''
c = MongoClient();
db = c.eyehome;
gazepoints_coll = db.gazepoints;
gazepoints = db.gazepoints.find();

''' State variables '''
previousDoc = activityStartDoc = None;
firstPoint = firstSession = True;
numSessions = 0L;

''' Helper methods '''
tz = pytz.timezone('America/New_York')
def getDate(ts):
	ts = ts/1000;
	return datetime.datetime.fromtimestamp(ts, tz).strftime('%m-%d-%Y %H:%M:%S');

def writeGazeAndApplicationCsv(startDoc, endDoc):
	application = None;
	previousTs = 0L;
	sessionStartTs = 0L;

	print "Writing gaze points and applications for this session..\n"
	if not os.path.exists(getDate(long(activityStartDoc['ts']))):
		os.makedirs(getDate(long(activityStartDoc['ts'])));

	startTime = long(startDoc['ts'])
	sessionStartTs = startDoc['ts']
	f_gaze = open(os.path.join(getDate(long(activityStartDoc['ts'])), 'gazepoints.csv'),'a')
	f_app = open(os.path.join(getDate(long(activityStartDoc['ts'])), 'app.csv'),'a')
	tmp_gazepoints = db.gazepoints.find();
	for doc in tmp_gazepoints:
		if long(doc['ts']) > long(endDoc['ts']):
			break;
		if 'duplicate' in doc:
			continue;
		if (long(doc['ts']) >= long(startDoc['ts'])) and (long(doc['ts']) <= long(endDoc['ts'])):
			f_gaze.write(str((long(doc['ts']) - startTime)/1000) + "sec," + str(doc['x']) + "," + str(doc['y']) + "\n");
			if application is None:
				application = doc['application']
				appStartTs = doc['ts']
			else:
				if (str(doc['application']) != str(application)):
					f_app.write(str((appStartTs - sessionStartTs)/1000) + "," + str((previousTs - sessionStartTs)/1000) + "," + application + "," + "\n")
					application = doc['application']
					appStartTs = doc['ts']

			previousTs = doc['ts']

	f_app.write(str((appStartTs - sessionStartTs)/1000) + "," + str((previousTs - sessionStartTs)/1000) + "," + application + "," + "\n")

	f_gaze.close()
	f_app.close()

def writeBlinksCsv(startDoc, endDoc):
	sessionStartTs = startDoc['ts']

	print "Writing blink information for this session..\n"

	f_blinks = open(os.path.join(getDate(long(activityStartDoc['ts'])), 'short_blinks.csv'),'a')
	tmp_blinks = db.blinks.find();
	for doc in tmp_blinks:
		if (doc['ts'] >= startDoc['ts']) and (doc['ts'] <= endDoc['ts']):
			f_blinks.write(str((doc['ts'] - sessionStartTs)/1000) + "," + "short_blink" + "," + "\n")

	f_long_blinks = open(os.path.join(getDate(long(activityStartDoc['ts'])), 'long_blinks.csv'),'a')
	tmp_long_blinks = db.long_blinks.find();
	for doc in tmp_long_blinks:
		if (doc['ts'] >= startDoc['ts']) and (doc['ts'] <= endDoc['ts']):
			f_long_blinks.write(str((doc['ts'] - sessionStartTs)/1000) + "," + "long_blink" + "," + "\n")

''' Main logic '''
for doc in gazepoints:

	'Ignore duplicates'
	if 'duplicate' in doc:
		continue;
	
	if firstPoint:
		firstPoint = False;
		activityStartDoc = doc;
		previousDoc = doc;
		continue;

	'''Find sessions spaced by 1 hour'''
	if (long(doc['ts']) - long(previousDoc['ts']) >  3600000):

		'''Find sessions that start with eyehome dashboard'''
		if activityStartDoc['application'] == "eyeHomeFourSides":
			numSessions += 1;
			activityLength = long(previousDoc['ts']) - long(activityStartDoc['ts']);
			print "\nSession: Length = " + str((activityLength * 1.0)/1000) + "s "
			print str(activityStartDoc['ts']) + " to " + str(previousDoc['ts'])
			print getDate(long(activityStartDoc['ts'])) + " - " + getDate(long(previousDoc['ts']))

			'''Write to files'''
			writeGazeAndApplicationCsv(activityStartDoc, previousDoc)
			writeBlinksCsv(activityStartDoc, previousDoc)

		activityStartDoc = doc;

	previousDoc = doc;

print numSessions




